var dataStorage = false;
try {
    dataStorage = JSON.parse(localStorage.totp);
} catch (E) {}

if (!dataStorage || typeof dataStorage !== 'object') {
    dataStorage = {};
}
dataStorage.counter = dataStorage.counter || 0;
if (!Array.isArray(dataStorage.keys)) {
    dataStorage.keys = [].concat(dataStorage.keys ||  []);
}

function addKey() {
    'use strict';

    var name = document.getElementById('name').value.trim();
    var key = document.getElementById('key').value.replace(/\s/g, '');

    if (!name) {
        alert('Enter account name');
        document.getElementById('name').focus();
        return;
    }

    if (key) {
        try {
            base32Decode(key);
        } catch (E) {
            alert(E.message);
            document.getElementById('key').focus();
            return;
        }
    } else {
        alert('Enter yout key');
        document.getElementById('key').focus();
        return;
    }

    document.getElementById('name').value = '';
    document.getElementById('key').value = '';

    var storage;
    try {
        storage = JSON.parse(localStorage.totp);
    } catch (E) {}

    if (!storage || typeof storage !== 'object') {
        storage = {};
    }
    storage.counter = storage.counter || 0;
    if (!Array.isArray(storage.keys)) {
        storage.keys = [].concat(storage.keys ||  []);
    }

    storage.keys.push({
        name: name,
        key: key,
        id: ++storage.counter
    });

    storage.keys.sort(function(a, b) {
        return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
    });

    dataStorage = storage;
    localStorage.totp = JSON.stringify(dataStorage);

    render();
}

function render() {
    'use strict';
    var container = document.getElementById('codes');

    dataStorage.keys.forEach(function(key) {
        var id = 'code_row_' + key.id;

        var element = document.getElementById(id);
        if (!element) {
            element = document.querySelector('#code-row .list-group-item').cloneNode(true);
            element.setAttribute('id', id);
            container.appendChild(element);

            element.querySelector('button.close').addEventListener('click', function() {
                if (confirm('Are you sure you want to delete this row?')) {
                    for (var i = 0; i < dataStorage.keys.length; i++) {
                        if (dataStorage.keys[i].id === key.id) {
                            dataStorage.keys.splice(i, 1);
                            break;
                        }
                    }
                    container.removeChild(element);
                    localStorage.totp = JSON.stringify(dataStorage);
                }
            }, false);
        }

        totp.generate(key.key, function(err, code) {
            try {
                element.querySelector('.code-name').innerHTML = key.name;
                element.querySelector('.code-key').innerHTML = err ? 'Error' : code;
            } catch (E) {}
        });
    });

}

setInterval(function() {
    var timepos = totp.timepos();
    document.getElementById('prg').value = timepos;
}, 100);

render();
setInterval(render, 1000);