window.totp = {

    _hmac: function(key, update, callback) {
        'use strict';

        if (typeof window.CryptoJS === 'object' && CryptoJS && CryptoJS.HmacSHA1) {
            console.log('Using CryptoJS');

            var key2 = CryptoJS.enc.Latin1.parse(Array.prototype.slice.call(key).map(function(c) {
                return String.fromCharCode(c);
            }).join(''));

            var update2 = CryptoJS.enc.Latin1.parse(Array.prototype.slice.call(update).map(function(c) {
                return String.fromCharCode(c);
            }).join(''));

            var hash = CryptoJS.HmacSHA1(update2, key2);

            var hash2 = [];
            hash = hash.toString();
            for (var i = 0; i < 20; i++) {
                hash2.push(parseInt(hash.substr(i * 2, 2), 16))
            }

            return callback(null, new Uint8Array(hash2));
        }
        console.log('Using WebCrypto');

        crypto.subtle.importKey('raw', key, {
            name: 'hmac',
            hash: {
                name: 'sha-1'
            }
        }, true, ['sign', 'verify']).then(function(result) {
            crypto.subtle.sign({
                name: 'hmac',
                hash: {
                    name: 'sha-1'
                }
            }, result, update).then(function(result) {
                callback(null, new Uint8Array(result));
            }, callback);
        }, callback);
    },

    _int64toUint8Array: function(intval) {
        'use strict';

        var arr = new Uint8Array(8);

        for (var i = 7; i >= 0; --i) {
            arr[i] = intval & (255);
            intval = intval >> 8;
        }

        return arr;
    },

    _hgen: function(key, counter, callback) {
        'use strict';

        var k = base32Decode(key || '');
        var p = 6;
        var b = totp._int64toUint8Array(counter || 0);

        totp._hmac(k, b, function(err, h) {
            if (err) {
                return callback(err);
            }

            // Truncate
            var offset = h[19] & 0xf;
            var v = (h[offset] & 0x7f) << 24 |
                (h[offset + 1] & 0xff) << 16 |
                (h[offset + 2] & 0xff) << 8 |
                (h[offset + 3] & 0xff);
            v = v + '';

            callback(null, v.substr(v.length - p, p));
        });
    },

    _tgen: function(key, curtime, step, callback) {
        'use strict';

        step = step || 30;
        var t = (curtime || Date.now()) / 1000;
        totp._hgen(key, Math.floor(t) / step, callback);
    },

    generate: function(key, callback) {
        totp._tgen(key, false, false, callback);
    },

    timepos: function() {
        var step = 30
        return (((Date.now() / 1000) % step) / step) * 100;
    }

};

/*

base32Decode is a stripped down version of the thirty-two module from https://www.npmjs.com/package/thirty-two
Original copyright follows.

Copyright (c) 2011, Chris Umbel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

function base32Decode(encoded) {
    'use strict';

    var byteTable = [
        // "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567"
        0xff, 0xff, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
        0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
        0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
        0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16,
        0x17, 0x18, 0x19, 0xff, 0xff, 0xff, 0xff, 0xff,
        0xff, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
        0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e,
        0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16,
        0x17, 0x18, 0x19, 0xff, 0xff, 0xff, 0xff, 0xff
    ];

    var shiftIndex = 0;
    var plainDigit = 0;
    var plainChar;
    var plainPos = 0;

    if (typeof encoded === 'string') {
        encoded = new TextEncoder('UTF-8').encode(encoded.toUpperCase());
    }

    var decoded = new Uint8Array(Math.ceil(encoded.length * 5 / 8));

    /* byte by byte isn't as pretty as octet by octet but tests a bit
        faster. will have to revisit. */
    for (var i = 0; i < encoded.length; i++) {
        if (encoded[i] == 0x3d) { //'='
            break;
        }

        var encodedByte = encoded[i] - 0x30;

        if (encodedByte < byteTable.length) {
            plainDigit = byteTable[encodedByte];

            if (shiftIndex <= 3) {
                shiftIndex = (shiftIndex + 5) % 8;

                if (shiftIndex === 0) {
                    plainChar |= plainDigit;
                    decoded[plainPos] = plainChar;
                    plainPos++;
                    plainChar = 0;
                } else {
                    plainChar |= 0xff & (plainDigit << (8 - shiftIndex));
                }
            } else {
                shiftIndex = (shiftIndex + 5) % 8;
                plainChar |= 0xff & (plainDigit >>> shiftIndex);
                decoded[plainPos] = plainChar;
                plainPos++;

                plainChar = 0xff & (plainDigit << (8 - shiftIndex));
            }
        } else {
            throw new Error('Invalid input - it is not base32 encoded string');
        }
    }
    return new Uint8Array(decoded.buffer.slice(0, plainPos));
}